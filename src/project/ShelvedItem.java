//Yasmeen Ashkar 4/29/22
//This class is a subclass of ConvenientStore that is specifically for items with no other requirements. 
//There is also no limit of quantity to be purchased for shelved items.
package project;

import java.util.ArrayList;

public class ShelvedItem extends ConvenientStore {
	
	public ShelvedItem() {
		super();
	}
	
	public ShelvedItem(String itemName, double price) {
		super(itemName, price);
	}
	
	@Override
	public int quantityLimit(int quantity, int stock) {
		if (quantity> stock || quantity > 25) {
			System.out.println("Quantity exceeded, please return some items");
			quantity=0;
		} else if (quantity<0) {
			System.out.println("Not a valid quantity");
			quantity =0;
		} else if (quantity <= quantity && quantity<=stock) {
			quantity = quantity;
		} else {
			System.out.println("Not a valid quantity");
			quantity =0;
		}
		return quantity;
	}
	
	@Override 
	public int checkStock(ArrayList<ConvenientStore> inventory, String itemName) {
		int count = 0;
		for (int i=0; i<inventory.size(); i++) {
			if (inventory.get(i).getItemName().equals(itemName)) {
				count = count+1;
			}
		}
		return count;
	}
	
	@Override
	public String toString() {
		return "Shelved item: " + super.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ConvenientStore)) {
			return false;
		} else if (!(obj instanceof ShelvedItem)) {
			return false;
		}
		
		ShelvedItem item = (ShelvedItem)obj;
		
		if (!(super.equals(item))) {
			return false;
		} 
		return true;
	}

}
