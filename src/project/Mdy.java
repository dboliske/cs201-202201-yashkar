//Yasmeen Ashkar 4/29/22
// this class is used to save a data given month day and year
package project;

public class Mdy {
	
	private int month;
	private int day;
	private int year;
	
	public Mdy() {
		month = 1;
		day = 1;
		year = 1970;
	}
	
	public Mdy(int month, int day, int year) {
		setMonth(month);
		setDay(day);
		setYear(year);
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month>0 && month<12) {
			this.month = month;
		} else {
			System.out.println("not a valid month");
		}
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day>0 && day<31) {
		this.day = day;
		} else {
			System.out.println("not a valid day");
		}
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		if (year>0) {
			this.year = year;
		} else {
			System.out.println("not a valid year");
		}
	}
	
	public String toString() {
		return (month<10?("0" + month): month) + "/" + (day<10?("0" + day): day) + "/" + year;
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Mdy)) {
			return false;
		}
		else if (!(obj instanceof Mdy)) {
			return false;
		}
		Mdy d = (Mdy)obj;
		
		if (this.month != d.getMonth()) {
			return false;
		} else if (this.day != d.getDay()) {
			return false;
		} else if (this.year != d.getYear()) {
			return false;
		} else {
			return true;
		}
	}

}
