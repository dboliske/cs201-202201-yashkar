//Yasmeen Ashkar 4/29/22
//This class is a subclass of ConvenientStore that is specifically for items with and expiration date.
//Besides price and name requirement, this class requires there to be a expiration date which has a type of class Date.
//This requires there to be a month day and year 
//There is also a limit of 20 items to be purchased for produce items.

package project;

import java.util.ArrayList;

public class ProduceItem extends ConvenientStore {
	
	private Mdy expDate;
	
	public ProduceItem(){
		super();
		expDate = new Mdy();
	}
	
	
	public ProduceItem(String itemName, double price, Mdy expDate) {
		super(itemName, price);
		this.expDate = expDate;
	}

	public Mdy getExpDate() {
		return expDate;
	}

	public void setExpDate(Mdy expDate) {
		this.expDate = expDate;
	}
	
	public int quantityLimit(int quantity, int stock) {
		if (quantity> stock || quantity>10) {
			System.out.println("Quantity exceeded, please return some items");
			quantity=0;
		} else if (quantity<0) {
			System.out.println("Not a valid quantity");
			quantity =0;
		} else {
			quantity = quantity;
		}
		return quantity;
	}
	
	@Override 
	public int checkStock(ArrayList<ConvenientStore> inventory, String itemName) {
		int count = 0;
		for (int i=0; i<inventory.size(); i++) {
			if (inventory.get(i).getItemName().equals(itemName)) {
				count = count+1;
			}
		}
		return count;
	}


	@Override
	public String toString() {
		return super.toString() + "," + expDate.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ConvenientStore)) {
			return false;
		} else if (!(obj instanceof ProduceItem)) {
			return false;
		}
		
		ProduceItem item = (ProduceItem)obj;
		
		if (!(super.equals(item))) {
			return false;
		} else if (!(this.expDate.equals(item.getExpDate()))) {
			return false;
		}
		return true;
	}
		
}
