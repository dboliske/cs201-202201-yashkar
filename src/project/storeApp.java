package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class storeApp {
	
	public static ArrayList<ConvenientStore> readInventory(String filename){
		//reads in file and saves in an array list given file name
		
		ArrayList<ConvenientStore> inventory = new ArrayList<ConvenientStore>();
		try {
			File f = new File("src/project/" + filename);
			Scanner input = new Scanner(f);
			
			while(input.hasNextLine()) {
				String line = input.nextLine();
				String[] data = line.split(",");
				
				String itemName = data[0];
				double price = Double.parseDouble(data[1]);
				
				try {
				String[] whichItem = data[2].split("/"); // need to check if item is a ageRestrictedItem or ProduceItem
				
				if (whichItem.length>1) { // if the third index could be split into a date it is a produce item
					int month = Integer.parseInt(whichItem[0]);
					int day = Integer.parseInt(whichItem[1]);
					int year = Integer.parseInt(whichItem[2]);
					Mdy expDate = new Mdy(month, day, year);
					ProduceItem a = new ProduceItem(itemName, price, expDate);
					inventory.add(a);
					
				} else if (whichItem.length == 1) { // if the third index can not be split it is an age 
					int ageRestriction = Integer.parseInt(whichItem[0]);
					AgeRestrictedItem a = new AgeRestrictedItem(itemName, price, ageRestriction);
					inventory.add(a);
				}
				
			} catch (ArrayIndexOutOfBoundsException oob) { // if there is no third index, it is a shelved item
				ShelvedItem a = new ShelvedItem(itemName, price);
				inventory.add(a);
			} catch (Exception e) {
				System.out.println("Error reading in iteam");
			}
			}
		}catch (FileNotFoundException fnf) {
			System.out.println("File not found!");
			
		}catch (Exception e) {
			System.out.println("Error reading in file");
		}
		
		return inventory;
	}
		
	public static int whichItem(ArrayList<ConvenientStore> inventory, String itemName) {
		//given item, this method specifies what specific type of item it is
		int val = searchItem(inventory, itemName);
		int type = 0;
		if (inventory.get(val) instanceof ProduceItem) {
			type = 1;
		} else if (inventory.get(val) instanceof AgeRestrictedItem) {
			type = 2;
		} else if (inventory.get(val) instanceof ShelvedItem) {
			type = 3;
		} else {
			System.out.println("Error! Item not in inventory");
		}
		return type;	
	}
	
	public static void menu(Scanner input, ArrayList<ConvenientStore> inventory) {

		// allows user to choose what they would like to do given menu options and an inventory
		boolean done = false;
		
		while (!done) {
			
			System.out.println("Select an option: ");
			System.out.println("1. add item");
			System.out.println("2. sell item ");
			System.out.println("3. search item");
			System.out.println("4. Update item");
			System.out.println("5. Exit");
			System.out.print("Choice: ");
			
			String choice = input.nextLine();
			
			switch (choice) {
			
			case "1": 
				addItem(inventory,input);
				break;
				
			case "2": 
				sellItem(inventory, input);
				break;
				
			case "3":
				System.out.print("Enter item you would like to search: ");
				String item = input.nextLine();
				searchItem(inventory, item );
				break;
				
			case "4":
				updateItem(inventory, input);
				break;
				
			case "5":
				done = true;
				break;
				
			default:
				System.out.println("Not a vaild choice");
			}
		}
		
	}
	
	public static ArrayList<ConvenientStore> addItem(ArrayList<ConvenientStore> inventory, Scanner input) {
		// adds and item to the end of inventory array list based on specified item by user
		boolean done = false;
		
		while(!done) {
			
			System.out.println("What kind of item is being added:");
			System.out.println("1. Produce Item");
			System.out.println("2. Age Restricted Item");
			System.out.println("3. Shelved Item");
			System.out.println("4. No items to add");
			System.out.print("Choice: ");
			
			String choice = input.nextLine();
			try {
			if (choice.equals("1")) {
				System.out.println("How many would you like to add?");
				int quantity = Integer.parseInt(input.nextLine());
				
				System.out.print("Enter Produce Item Name: ");
				String itemName = input.nextLine();
				
				System.out.print("Enter Price: ");
				double price = Double.parseDouble(input.nextLine());
				
				System.out.print("Enter expiration date in mm/dd/yyyy format: ");
				String[] date = input.nextLine().split("/");
				int month = Integer.parseInt(date[0]);
				int day = Integer.parseInt(date[1]);
				int year = Integer.parseInt(date[2]);
				Mdy expDate = new Mdy(month, day, year);
				
				ProduceItem pi = new ProduceItem(itemName, price, expDate);
				for (int i = 0; i<quantity; i++) {
					inventory.add(pi);
				}
			} else if (choice.equals("2")) {
				System.out.println("How many would you like to add?");
				int quantity = Integer.parseInt(input.nextLine());
				
				System.out.print("Enter Age Restricted Item Name: ");
				String itemName = input.nextLine();
				
				System.out.print("Enter Price: ");
				double price = Double.parseDouble(input.nextLine());
				
				System.out.print("Enter Age Restriction: ");
				int ageRestriction = Integer.parseInt(input.nextLine());
				
				AgeRestrictedItem ari = new AgeRestrictedItem(itemName, price, ageRestriction);
				for (int i = 0; i<quantity; i++) {
					inventory.add(ari);
				}
			} else if (choice.equals("3")) {
				System.out.println("How many would you like to add?");
				int quantity = Integer.parseInt(input.nextLine());
				
				System.out.print("Enter Shelved Item Name: ");
				String itemName = input.nextLine();
				
				System.out.print("Enter Price: ");
				double price = Double.parseDouble(input.nextLine());
				
				ShelvedItem si = new ShelvedItem(itemName, price);
				for (int i = 0; i<quantity; i++) {
					inventory.add(si);
				}
			} else if (choice.equals("4")) {
				System.out.println("No new items");
				done = true;
			} else {
				System.out.println("Not a valid choice.");
			}
			}catch (Exception e) {
				System.out.println("Error Reading in added item");
			}
		}
		return inventory;
	}
	
	public static ArrayList<ConvenientStore> sellItem(ArrayList<ConvenientStore> inventory, Scanner input) {
		
		// sells an items customer chooses to purchase after adding to cart 
		// makes sure the customer does not exceed quantity limit
		boolean done = false;
		
		ArrayList<String> cart = new ArrayList<String>();
		

		while(!done) {
			System.out.print("Would you like to add and item to cart? (Enter yes or no): ");
			String ans = input.nextLine();
			
			if (ans.toLowerCase().equals("y") || ans.toLowerCase().equals("yes")) {
				System.out.print("Item being added to cart: ");
				String item = input.nextLine().toLowerCase();
				int itemNum = searchItem(inventory, item);
				
				int quantity = 0;
			
				while (quantity == 0 && itemNum < inventory.size()) {
					try {
						System.out.print("How many would you like to add? ");
						int desiredQuantity = Integer.parseInt(input.nextLine());
						
						ConvenientStore cartItem = inventory.get(itemNum);
						
						int stock = cartItem.checkStock(inventory, cartItem.getItemName());
						
						quantity = inventory.get(itemNum).quantityLimit(desiredQuantity, stock);
					} catch (Exception e) {
						System.out.println("Error with desired quantity. Please enter valid quantity");
					}
				}
				
				if (itemNum +1 < inventory.size()) {
					for (int i =0; i<quantity; i++) {
						cart.add(item);
					}
				}
			} else if (ans.toLowerCase().equals("n") || ans.toLowerCase().equals("no")) {
				done = true;
			} else {
				System.out.println("Not a valid answer");
			}
				
		}
		
		for (int i=0; i<cart.size(); i++) {
			System.out.print("Are you sure you want to purchase " + cart.get(i) + "? ");
			String ans = input.nextLine();
			
			if (ans.toLowerCase().equals("y") || ans.toLowerCase().equals("yes")) {
				int itemNum = searchItem(inventory, cart.get(i));
				inventory.remove(itemNum);
			}
		}
		return inventory;
	}
	
	public static int searchItem(ArrayList<ConvenientStore> inventory, String item) {
		// searched item using sequential seatch and calls items toString method
		int count = 0;
		boolean found = false;
		
		while(!found && count < inventory.size()) {
			if (inventory.get(count).getItemName().equals(item)) {
				System.out.println(inventory.get(count).toString());
				found = true;
			} else {
				count++;
			}
		}
		if (found == false) {
			System.out.println("Item is not in inventory!");
		}
			return count;
	}
	
	public static ArrayList<ConvenientStore> updateItem(ArrayList<ConvenientStore> inventory, Scanner input) {
		//updates item based on input from user
		
		try {
			System.out.print("Name of item being updated: ");
			String item = input.nextLine();
			
			int type = whichItem(inventory, item);
				if (type == 1) {
					System.out.println("Enter new: item name, price, expiration date (mm/dd/yyyy)");
					String[] update = input.nextLine().split(", ");
					String newName = update[0]; 
					double newPrice = Double.parseDouble(update[1]); 
					String[] nDate = update[2].split("/");
					int newMonth = Integer.parseInt(nDate[0]);
					int newDay = Integer.parseInt(nDate[1]);
					int newYear = Integer.parseInt(nDate[2]);
					Mdy newExpDate =  new Mdy(newMonth, newDay, newYear);
					ProduceItem updated = new ProduceItem(newName, newPrice, newExpDate);
					for(int i=0; i<inventory.size(); i++) {
						if (inventory.get(i).getItemName().equals(item)) {
							inventory.set(i, updated);
						}
					}
				} else if (type == 2) {
					System.out.println("Enter: item name, price, ageRestriction");
					String[] update = input.nextLine().split(",");
					String newName = update[0]; 
					double newPrice = Double.parseDouble(update[1]); 
					int newAgeRestriction = Integer.parseInt(update[2]);
					AgeRestrictedItem updated = new AgeRestrictedItem(newName, newPrice, newAgeRestriction);
					for(int i=0; i<inventory.size(); i++) {
						if (inventory.get(i).getItemName().equals(item)) {
							inventory.set(i, updated);
						}
					}
				} else if (type == 3) {
					System.out.println("Enter: item name, price");
					String[] update = input.nextLine().split(",");
					String newName = update[0]; 
					double newPrice = Double.parseDouble(update[1]); 
					ShelvedItem updated = new ShelvedItem(newName, newPrice);
					for(int i=0; i<inventory.size(); i++) {
						if (inventory.get(i).getItemName().equals(item)) {
							inventory.set(i, updated);
						}
					}
				} else {
					System.out.println("Error. Not a valid item");
				}
			}catch (Exception e) {
				System.out.println("Error reading in value! Returning to menu...");
			}
				
			return inventory;
		}
	
	public static void saveFile(ArrayList<ConvenientStore> inventory, Scanner input) {
		// saves file to .txt 
		System.out.print("Enter .txt file name to save file: "); //ask user for file name
		String fileName = input.nextLine();
		try {
			FileWriter f = new FileWriter("src/project/" + fileName); //use name inputed for file directory
			for (int count = 0; count <inventory.size(); count++) {
				f.write(inventory.get(count) + "\n");
			}
			f.flush();
			f.close();
		} catch(IOException e) {
			
		}
	}

	public static void main(String[] args) {
		
		//calls methods to start program
		
		Scanner input = new Scanner(System.in);

		System.out.print("Enter filename: ");
		String filename = input.nextLine();
		
		ArrayList<ConvenientStore> inventory = readInventory(filename);
	
		menu(input, inventory);
		
		boolean done = false;
		while (!done) {
			System.out.println("Would you like to save this file? ");
			String ans = input.nextLine();
			if (ans.toLowerCase().equals("yes") || ans.toLowerCase().equals("y")) {
			saveFile(inventory, input);
			done = true;
			} else if ((ans.toLowerCase().equals("no") || ans.toLowerCase().equals("n"))) {
				done = true;
			} else {
				done = false;
			}
		}
		
		System.out.println("Goodbye");
	}
}
	

