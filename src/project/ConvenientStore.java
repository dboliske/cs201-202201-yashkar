//Yasmeen Ashkar 4/29/22
//This class is a superclass of ConvenientStore that includes all items. Require there to be a price and name.
package project;

import java.util.ArrayList;

public class ConvenientStore {

	protected String itemName;
	private double price;
	
	public ConvenientStore() {
		itemName = "none";
		price = 0.0;
		
	}
	
	public ConvenientStore(String itemName, double price) {
		this.itemName = itemName;
		setPrice(price);
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if (price>0) {
			this.price = price;
		} else {
			System.out.println("not a valid price");
		}
	}
	
	public int quantityLimit(int quantity, int stock) {
		if (quantity>25) {
			System.out.println("Quantity exceeded, please return some items");
			quantity=0;
		} else if (quantity<0) {
			System.out.println("Not a valid quantity");
			quantity =0;
		} else {
			quantity = quantity;
		}
		return quantity;
	}
	
	public int checkStock(ArrayList<ConvenientStore> inventory, String itemName) {
		int count = 0;
		for (int i=0; i<inventory.size(); i++) {
			if (inventory.get(i).getItemName().equals(itemName)) {
				count = count+1;
			}
		}
		return count;
	}
	
	@Override 
	public String toString() {
		return itemName + "," + price;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ConvenientStore)) {
			return false;
		}
		
		ConvenientStore item = (ConvenientStore)obj;
		 
		if (!(this.itemName.equals(item.getItemName()))) {
			return false;
		} else if (!(this.price == item.getPrice())) {
				return false;
		}
		return true;
	}
	
}
