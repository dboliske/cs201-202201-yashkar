//Yasmeen Ashkar 4/29/22
//This class is a subclass of ConvenientStore that is specifically for items with and age restriction. 
//Besides price and name requirement, this class requires there to be a age restriction.
//There is also a limit of 10 items to be purchased for age restricted items.
package project;

import java.util.ArrayList;

public class AgeRestrictedItem extends ConvenientStore {
	
	private int ageRestriction;
	
	public AgeRestrictedItem() {
		super();
		ageRestriction = 18;
	}
	
	public AgeRestrictedItem(String itemName, double price, int ageRestriction) {
		super(itemName, price);
		this.ageRestriction = ageRestriction;
	}

	public int getAgeRestriction() {
		return ageRestriction;
	}

	public void setAgeRestriction(int ageRestriction) {
		if (ageRestriction > 0 && ageRestriction<100) {
			this.ageRestriction = ageRestriction;
		} else {
			System.out.println("not a valid age");
		}
	}
	
	public int quantityLimit(int quantity, int stock) {
		if (quantity> stock ||quantity>10) {
			System.out.println("Quantity exceeded, please return some items");
			quantity=0;
		} else if (quantity<0) {
			System.out.println("Not a valid quantity");
			quantity =0;
		} else {
			quantity = quantity;
		}
		return quantity;
	}
	
	@Override 
	public int checkStock(ArrayList<ConvenientStore> inventory, String itemName) {
		int count = 0;
		for (int i=0; i<inventory.size(); i++) {
			if (inventory.get(i).getItemName().equals(itemName)) {
				count = count+1;
			}
		}
		return count;
	}
	
	@Override
	public String toString() {
		return super.toString() + ","  + ageRestriction;
	}
	

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ConvenientStore)) {
			return false;
		} else if (!(obj instanceof AgeRestrictedItem)) {
			return false;
		}
		
		AgeRestrictedItem item = (AgeRestrictedItem)obj;
		
		if (!(super.equals(item))) {
			return false;
		} else if (!(this.ageRestriction==item.getAgeRestriction())) {
			return false;
		}
		return true;
	}

}
