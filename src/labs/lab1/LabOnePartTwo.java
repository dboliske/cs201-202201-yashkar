package labs.lab1;

import java.util.Scanner;

public class LabOnePartTwo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//Performing arithmetic operations 
		int myAge = 20;
		int dadsAge = 46;
		int birthYear = 2001;
		int myHeight = 63; //height in inches
		
		//my age subtracted from fathers age
		int diff = dadsAge - myAge;
		System.out.println("The difference between me an my father's age is " + diff);
		
		//birth year times 2 
		int newbY = birthYear*2;
		System.out.println("My birth year times 2 is " + newbY);
		
		//Height in inches to cm 
		double cmH= ((double)(myHeight))* 2.54;
		System.out.println("My height in centimeters is " + cmH);
		
		//Height in inches to feet and inches
		int ftH = myHeight/12; // how many feet 
		int inH= myHeight%12; // how many inches
		System.out.println("My height is " + ftH + " feet " + "and " + inH + " inches");
		
		input.close();
	}

}
