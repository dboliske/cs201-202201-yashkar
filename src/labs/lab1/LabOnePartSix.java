package labs.lab1;

import java.util.Scanner;

public class LabOnePartSix {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//inches to centimeters 6
		System.out.print("Enter inches: ");
		
		double inches = Double.parseDouble(input.nextLine());
		
		double cm = inches * 2.54 ;
		
		System.out.println(inches + " is equal to " + cm + " centimeters");
		
		input.close();
		
		//Test Table (Fahrenheit to Celsius):
			//Input inches  	Output centimeters
				// 36 				 91.44
				// 72				 182.88
				// 64				 162.56
	}

}
