package labs.lab1;

import java.util.Scanner;

public class LabOnePartOne {

	public static void main(String[] args) {
		//Reading input and writing output 1
		Scanner input = new Scanner(System.in);
		
		System.out.print("Name: "); //prompt user for name
		
		String value = input.nextLine(); //save name
		
		System.out.println("Echo: " + value); //echo name to console
		
		input.close();
	}

}
