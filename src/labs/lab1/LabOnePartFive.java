package labs.lab1;

import java.util.Scanner;

public class LabOnePartFive {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		// Box Area 
		System.out.print("length in inches: ");
		double length = Double.parseDouble(input.nextLine());
		
		System.out.print("width in inches: ");
		double width = Double.parseDouble(input.nextLine());
		
		System.out.print("height in inches: ");
		double height = Double.parseDouble(input.nextLine());
		
		//convert inches to feet
		double l = length/((double)(12));
		double w = width/((double)(12));
		double h = height/((double)(12));
		
		//Calculate surface area of box to find amount of wood needed
		double SA = (2*l) + (2*w) + (2*h);
		
		System.out.println("The amount of wood needed to make the box is " + SA + " square feet");
		
		input.close();
		
		//Test Table:
			//Input length 	Input width 	Input height 	Output Surface Area
				// 5 			 5 				 5 				 2.5 
				// 6 			 6 				 6 				 3.0
				// 4 			 5 			 	 6 			 	 2.5 
	}

}
