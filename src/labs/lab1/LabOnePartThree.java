package labs.lab1;

import java.util.Scanner;

public class LabOnePartThree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
	
		//Getting a char from keyboard input 
		System.out.print("Name: "); //prompt user for name
		
		char initial = input.nextLine().charAt(0); //get first letter of name 
		
		System.out.println("First initial is " + initial); //output initial to user
		
		input.close();
	}

}
