package labs.lab1;

import java.util.Scanner;

public class LabOnePartFour {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		// Temperature fahrenheit to celsius 
		System.out.print("Temperature in Fahrenheit: "); //Prompt user for temperature in Farenheit 
		double tempF = Double.parseDouble(input.nextLine()); 
		
		double tempC =  ((tempF - 32) * (((double)(5)) / ((double)(9)))); //convert Fahrenheit to Celsius 
		
		System.out.println(tempF + " degrees Farenheit is " + tempC + " degrees Celsius");
		
		//Temperature Celsius to Fahrenheit
		System.out.print("Temperature in Celsius: "); //Prompt user for Temperature in Celsius
		double celTemp = Double.parseDouble(input.nextLine());
		
		double fahrTemp = (celTemp * (((double)(9))/(double)(5))) + 32; //Convert Celsius to Farenheit
		
		System.out.println(celTemp + " degrees Celsius is " + fahrTemp + " degrees Fahrenheit");
		
		input.close();
		
		//Test Table (Fahrenheit to Celsius):
			//Input Temp in F 	Output Temp in C
				// 0 				 -17.77777777777778
				// 32 				 0.0
				// 90 				 32.22222222222222
				
		//Test Table (Celsius to Fahrenheit):
			//Input Temp in C 	Output Temp in F
				// 0 				 32.0
				// 20 				 68.0
				// 40 				 104.0
		
		// I am satisfied with the way my program works as I am getting all the correct conversions 
		// from both Celsius to Fahrenheit and Fahrenheit to Celsius
		
	}

}
