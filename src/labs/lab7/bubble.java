package labs.lab7;

public class bubble {
	
	public static void sort(int[] data) {
		
		boolean sorted = false;
		
		while (sorted == false) {
			sorted = true;
			for (int i=0; i<data.length - 1; i++) {
				if (data[i]>data[i+1]) {
					int temp = data[i];
					data[i] = data[i+1];
					data[i+1] = temp;
					sorted = false;	
				}
			}
		}
		
		
	}

	public static void main(String[] args) {
		
		int[] data = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		
		sort(data);
		
		for (int i = 0; i<data.length; i++) {
			System.out.print(data[i] + " ");
		}

	}

}
