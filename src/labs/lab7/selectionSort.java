package labs.lab7;

public class selectionSort {
	
	public static void sort(double[] data) {
		
		for (int i=0; i<data.length; i++) {
			int min =i;
			
			for (int j=i+1; j<data.length; j++) {
				if (data[min]>data[j]) {
					min = j;
				}
			}
			
			if (min != i) {
				double temp = data[i];
				data[i] = data[min];
				data[min]=temp;
			}
		}
		
	}
	

	public static void main(String[] args) {
		
		double [] data = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		sort(data);
		
		for (int i = 0; i<data.length; i++) {
			System.out.print(data[i] + " ");
		}

	}

}
