package labs.lab7;

import java.util.Scanner;

public class binarySearch {
	
	public static int search(String[] data, String item, int start, int end) {
		
		int middle = (end+start)/2;
		
		if (item.equals(data[middle])) {
			return middle;
		} else if (start >= end) {
			return -1;
		}
		
		
		if (item.compareTo(data[middle])<0) {
			end = middle;
		} else if (item.compareTo(data[middle])>0) {
			start = middle +1;
		}
		return search(data, item, start, end);
	}

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Enter programming language: ");
		String item = input.nextLine();

		String[] data = {"c", "html", "java", "python", "ruby", "scala"};
		
		int index = search(data, item, 0, data.length);
		
		if (index<0) {
			System.out.println(item + " not found");
		} else {
			System.out.println(item + " occurs at index "  + index);
		}

	}

}
