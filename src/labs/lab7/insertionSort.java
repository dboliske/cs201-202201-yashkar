package labs.lab7;

public class insertionSort {
	
	public static void sort(String[] data) {
		
		for (int i = 1; i<data.length; i++) {
			int j=i;
			while (j>0 && (data[j].compareTo(data[j-1])<0)) {
				String temp = data[j-1];
				data[j-1] = data[j];
				data[j] = temp;
				j--;
			}
			
		}
	}

	public static void main(String[] args) {
		
		String[] data = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		sort(data);
		
		for (int i = 0; i<data.length; i++) {
			System.out.print(data[i] + " ");
		}
	}

}
