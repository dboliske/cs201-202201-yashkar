package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class exerciseTwo {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int i=0;
		
		double[] values = new double[2]; 
		System.out.print("Enter value: ");
		String choice = input.nextLine();
		
		//keep reading values until user enter done
		while (!choice.equalsIgnoreCase("done")) {
			
			//if array is too small, make it bigger
			if (i == values.length) {
				double[] bigger = new double[values.length + 1];
				for (int n=0; n< values.length; n++) {
					bigger[n] = values[n];
				}
				values = bigger;
				bigger = null;
			}
			
			values[i] = Integer.parseInt(choice);
			System.out.print("Enter value: ");
			choice = input.nextLine();
			i++;
			}
		
		System.out.print("Enter .txt file name"); //ask user for file name
		String fileName = input.nextLine();
		try {
			FileWriter f = new FileWriter("src/labs/lab3/" + fileName); //use name inputed for file directory
			for (int count = 0; count <values.length; count++) {
				f.write(values[count] + "\n");
			}
			f.flush();
			f.close();
		} catch(IOException e) {
			
		}
	}
}
		
			


	


