package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class averageGrade {

	public static void main(String[] args) throws IOException {
		
		File f = new File("src/labs/lab3/grades.csv"); 
		
		Scanner input = new Scanner(f); //read in file 
		
		//initialize variables needed
		int count = 0; 
		int[] grades = new int[2];
		double sum = 0; 
		
		while (input.hasNextLine()) {
		
		String line = input.nextLine();
		
		String[] data = line.split(","); //separate name of student and grade
		
		//make a array bigger if there are more grades that need to be read in 
		if (count == grades.length) {
			int[] bigger = new int[grades.length + 1];
			for (int i=0; i< grades.length; i++) {
				bigger[i] = grades[i];
			}
			grades = bigger;
			bigger = null;
		}
		grades[count]= Integer.parseInt(data[1]); //read the grade value
		sum= sum + grades[count]; //add each new grade to sum 
		count++;	
	}
		System.out.println("The average of the grades is " + sum/count);
		
	}
}
