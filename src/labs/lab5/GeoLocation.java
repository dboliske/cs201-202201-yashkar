package labs.lab5;

public class GeoLocation {
	
	private double lat;
	private double lng;
	
	public GeoLocation() {
		lat = 0;
		lng = 0;
	}
	
	public GeoLocation(double lat, double lng) {
		this.lat = 0;
		setLat(lat);
		this.lng=0;
		setLng(lng);
		
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
		
	}
	
	public void setLat(double lat) {
		if(lat >= -90 && lat <= 90) {
			this.lat = lat;
		}
		
	}
	
	public void setLng(double lng) {
		if((lng>=-180) && (lng<=180)) {
			this.lng = lng;
		}
	}
	
	public String toString() {
		return "GeoLocation is (" + lat + ", " + lng + ")";
		
	}
	
	public boolean validLat(double lat) {
		if((lat>=-90) && (lat<=90)) {
			return true; 
		}
		return false;
		
		}
	
	public boolean validLng(double lng) {
		if((lng>=-180) && (lng<=180)) {
			return true; 
		}
		return false;
		
	}
	
	public boolean equals(GeoLocation gL) {
		if (this.lat != gL.getLat()) {
			return false;
		} else if (this.lng != gL.getLng()) {
			return false;
		}
		
		return true;
	}
	
	
	public double calcDistance(GeoLocation gL) {
		
		double dist = Math.sqrt(Math.pow((this.lat - gL.getLat()), 2) + Math.pow((this.lng - gL.getLng()), 2));
		
		return dist;
	}
	
	
}


