package labs.lab5;

public class CTAStation extends GeoLocation {
	
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation() {
		super();
		name = "unknown";
		location = "elevated";
		wheelchair = false;
		open = false;
	}
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super(lat,lng);
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}

	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof GeoLocation)) {
			return false;
		}
		else if (!(obj instanceof CTAStation)) {
			return false;
		}
		
		CTAStation cta = (CTAStation)obj;
		
		if (!this.name.equals(cta.getName())) {
			return false;
		} else if (!super.equals(cta)) {
			return false;
		} else if (!this.location.equals(cta.getLocation())) {
			return false;
		} else if (this.wheelchair != cta.hasWheelchair()) {
			return false;
		} else if (this.open != cta.isOpen()) {
			return false;
		}
		
		return true;
	}
	
	@Override 
	public String toString() {
		return super.toString() + " for " + name + " with location " + location;
	}
	
}
