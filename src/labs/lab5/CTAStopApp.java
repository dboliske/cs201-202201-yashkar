package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp {
	
	public static CTAStation[] readFile(String filename) {
		
		CTAStation[] CTAData = new CTAStation[10];
		
		int count = 0;
		try {
			File f =  new File(filename);
			Scanner input = new Scanner(f);
			
			while (input.hasNextLine()) {
				String line = input.nextLine();
				if (count!=0) {
				String[] data = line.split(",");
				
				if (CTAData.length == count-1) {
						CTAStation[] temp = new CTAStation[(CTAData.length)*2];
						for (int i=0; i<CTAData.length; i++) {
							temp[i] = CTAData[i];
						}
						CTAData = temp;
						temp = null;
				}
				CTAData[count-1] = new CTAStation(data[0], Double.parseDouble(data[1]), Double.parseDouble(data[2]), data[3], Boolean.parseBoolean(data[4]), Boolean.parseBoolean(data[5]));  
				}
				count++;
			}
				
			if (count-1<CTAData.length) {
				CTAStation[] temp = new CTAStation[count-1];
				for (int i = 0; i<count-1; i++) {
					temp[i] = CTAData[i];
				}
				CTAData = temp;
				temp = null;
			}
			
			input.close();
		} catch (FileNotFoundException fnf) {
		// Do Nothing 
		} catch (Exception e) {
			
			System.out.println("Error Reading File");
		}
		
		return CTAData;
		
	}	
	
	public static CTAStation[] menu(Scanner input, CTAStation[] data) {
		
		boolean done = true;
		
		while (done) {
			
			System.out.println("Select an option: ");
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations with/without Wheelchair Access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			
			String choice = input.nextLine();
			
			switch (choice) {
			
			case "1": 
				displayStationNames(data);
				break;
				
			case "2": 
				displayByWheelchair(data, input);
				break;
				
			case "3":
				displayNearest(data,input);
				break;
				
			case "4":
				done = false;
				break;
				
			default:
				System.out.println("Not a vaild choice");
			}
		}
		return data;
	}
	
	public static void displayStationNames(CTAStation[] data) {
		
		for (int i = 0; i < data.length; i++) {
			System.out.println(data[i].getName()); 
		}
	}
	
	public static void displayByWheelchair(CTAStation[] data, Scanner input) {
		
		boolean done = true;
		
		while (done) {
			System.out.println("Enter 'y' or 'n' for accessibility ");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			int count = 0;
			
			if (choice.toLowerCase().equals("y") || choice.toLowerCase().equals("yes")) {
				boolean access = true;
				for (int i = 0; i<data.length; i++) {
					if (data[i].hasWheelchair() == access) {
						System.out.println(data[i].getName() + " has wheelchair accessibility");
						count++;
					} 
					if (count == 0) {
						System.out.println("No stations found");
					}
				}
				done = false;
			} else if (choice.toLowerCase().equals("n") || choice.toLowerCase().equals("no")) {
				boolean access = false;
				count++;
				for (int i = 0; i<data.length; i++) {
					if (data[i].hasWheelchair() == access) {
						System.out.println(data[i].getName() + " does not have wheelchair accessibility");
					} 
					if (count==0) {
						System.out.println("No stations found");
					}
				}
				done = false;
			} else {
				System.out.println("Not a valid choice");
			}
		}
	}
		
	public static void displayNearest(CTAStation[] data, Scanner input) {
		
		System.out.print("Enter Latitude: ");
		double lat = Double.parseDouble(input.nextLine());
		
		System.out.print("Enter longitude: ");
		double lng = Double.parseDouble(input.nextLine());
		
		GeoLocation gl1 = new GeoLocation(lat,lng);
		
		double[] distances = new double[10];
		int count = 0;
		
		for (int i = 0; i < data.length; i++) {
			GeoLocation gl2 = new GeoLocation(data[i].getLat(), data[i].getLng());
			double dist = gl1.calcDistance(gl2);
			if (count == distances.length) {
				double[] temp = new double[(distances.length)*2];
				for (int x=0; x<distances.length; x++) {
					temp[x] = distances[x];
				}
				distances = temp;
				temp = null;
			}
			distances[count]=dist;
			count++;
		}
		
		if (count<distances.length) {
			double[] temp = new double[count];
			for (int i = 0; i<count; i++) {
				temp[i] = distances[i];
			}
			distances = temp;
			temp = null;
		}
		
		double nearest = distances[0];
		int track = 0;
		for (int i=0; i<distances.length; i++) {
			if (distances[i] < nearest) {
				nearest = distances[i];
				track=i;
			}
		}
		
		System.out.println("Nearest station is " + data[track].getName());
		
	}

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Enter filename: ");
		String filename = input.nextLine();
		
		CTAStation[] data = readFile(filename);
		
		data = menu(input, data);
		
		input.close();

	}

}
