package labs.lab4;

public class potionApp {

	public static void main(String[] args) {
		//default constructor
		Potion p1 = new Potion();
		System.out.println(p1);
		
		//nondefault constructor
		Potion p2 = new Potion("magic", 4.5);
		System.out.println(p2);
		
		//checking validity of strength value
		System.out.println(p2.validStrength(p2.getStrength()));
		
		//checking equality
		System.out.println(p1.equals(p2));
		
		//setting equivalent values for p1 and checking equality
		p1.setName(p2.getName());
		p1.setStrength(p2.getStrength());
		System.out.println(p1);
		
		System.out.println(p1.equals(p2));
		
		//check to see if invalid input changes instance variable
		p2.setStrength(11);
		System.out.println(p2);
	}

}
