package labs.lab4;

public class phoneNumberApp {

	public static void main(String[] args) {
		//default constructor phone number
		PhoneNumber pN1 = new PhoneNumber();
		System.out.println(pN1.toString());
		
		//nondefault constructor phone number
		PhoneNumber pN2 = new PhoneNumber("1", "708", "4309745");
		System.out.println(pN2.toString());
		
		//check for equality
		System.out.println(pN1.equals(pN2));
		
		//check validity of numbers
		System.out.println(pN2.validAreaCode(pN2.getAreaCode()));
		System.out.println(pN2.validNumber(pN2.getNumber()));
		
		//setting default to pN2 and checking for equality
		pN1.setCountryCode(pN2.getCountryCode());
		pN1.setAreaCode(pN2.getAreaCode());
		pN1.setNumber(pN2.getNumber());
		
		System.out.println(pN1.toString());
		
		System.out.println(pN1.equals(pN2));
		
		//check to see if invalid input changes instance variable
		pN2.setNumber("54367");
		System.out.println(pN2.toString());
		
	}

}
