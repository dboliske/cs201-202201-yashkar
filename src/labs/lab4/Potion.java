package labs.lab4;

public class Potion {

	private String name;
	private double strength;
	
	public Potion() {
		name = "unknown";
		strength = 0;
	}
	
	public Potion(String name, double strength) {
		this.name = name;
		
		this.strength = 0;
		setStrength(strength);
	}
	
	public String getName() {
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setStrength(double strength) {
		if (strength>=0 && strength<=10) {
			this.strength = strength;
		}
	}
	
	public String toString() {
		return name + " has a strength of " + strength;
	}
	
	public boolean validStrength(double strength) {
		if (strength>=0 && strength<=10) {
			return true;
		}
		return false;
	}
	
	public boolean equals(Potion p) {
		if (name.equals(p.getName())) {
			if (strength == p.getStrength()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}
