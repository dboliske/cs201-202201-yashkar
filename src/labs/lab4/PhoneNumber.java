package labs.lab4;

public class PhoneNumber {
	
	private String countryCode;
	private String areaCode;
	private String number;
	
	public PhoneNumber() {
		countryCode = "1";
		areaCode = "708";
		number = "1234567";		
	}
	
	public PhoneNumber(String countryCode, String areaCode, String number) {
		this.countryCode = countryCode;
		this.areaCode = "708";
		setAreaCode(areaCode);
		this.number = "1234567";
		setNumber(number);
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}

	public void setCountryCode(String cCode) {
		countryCode =cCode;
	}
	
	public void setAreaCode(String aCode) {
		if (aCode.length() == 3) {
			areaCode = aCode;
		}
	}
	
	public void setNumber(String number) {
		if (number.length() == 7) {
			this.number = number;
		}
	}
	
	public String toString() {
		return "Phone Number is " + countryCode + "-" + areaCode + "-" + number;
	}
	
	public boolean validAreaCode(String aCode) {
		if (aCode.length() != 3) {
		return false;
		}
	return true;
	}
	
	public boolean validNumber(String number) {
		if (number.length() != 7) {
		return false;
		}
	return true;
	}
	
	public boolean equals(PhoneNumber pN) {
		if (countryCode.equals(pN.getCountryCode())) {
			if (areaCode.equals(pN.getAreaCode())) {
				if (number.equals(pN.getNumber())) {
					return true;
				} else {
					return false;
				} 
			} else {
					return false;
			}
		} else {
			return false;
		}
	}
	
}

