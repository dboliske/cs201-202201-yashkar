package labs.lab4;

public class geoLocationApp {

	public static void main(String[] args) {
		
		GeoLocation gL1 = new GeoLocation(); //default instances
		
		System.out.println("GeoLocation is 1 (" + gL1.getLat() + ", " + gL1.getLng() + ")" ); //calling accessor
		
		GeoLocation gL2 = new GeoLocation(45, 130); //non default instances
		
		System.out.println("GeoLocation 2 is (" + gL2.getLat() + ", " + gL2.getLng() + ")" ); //calling accessor
		
		//Check if values entered in gL2 are valid
		System.out.println(gL2.validLat(gL2.getLat()));
		System.out.println(gL2.validLng(gL2.getLng()));
		
		//checking equality
		System.out.println(gL1.equals(gL2));
		
		//setting equivalent values for gL1 and checking equality
		gL1.setLat(gL2.getLat());
		gL1.setLng(gL2.getLng());
		System.out.println("GeoLocation 1 is (" + gL1.getLat() + ", " + gL1.getLng() + ")" ); //calling accessor
		System.out.println(gL1.equals(gL2));
		
		//check to see if invalid input changes instance variable
		gL2.setLat(-100);
		System.out.println("GeoLocation 2 is (" + gL2.getLat() + ", " + gL2.getLng() + ")" ); //calling accessor
	}

}
