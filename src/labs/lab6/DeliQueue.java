package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliQueue {
	
	public static void menu(Scanner input, ArrayList<String> data) {
		
		boolean done = true;
		
		while (done) {
			
			System.out.println("Select an option: ");
			System.out.println("1. Add Customer");
			System.out.println("2. Help Customer");
			System.out.println("3. Exit");
			System.out.print("Choice: ");
			
			String choice = input.nextLine();
			
			switch (choice) {
			
			case "1": 
				int index = addCustomer(input,data);
				int num = index+1;
				System.out.println(data.get(index) + " is number " + num + " in line.");
				break;
				
			case "2":
				helpCustomer(data);
				break;
				
			case "3":
				done = false;
				break;
				
			default:
				System.out.println("Not a vaild choice");
			}
		}
	}
	
	public static int addCustomer(Scanner input, ArrayList<String> data) {
		
		System.out.print("Name of new customer: ");
		String name = input.nextLine();
		
		data.add(name);
		
		return data.size()-1;
	}
	
	public static void helpCustomer(ArrayList<String> data) {
		
		try {
			String name = data.remove(0);
			System.out.println("The next customer being helped is " + name);
			
		} catch (IndexOutOfBoundsException iob) {
			System.out.println("There are no customers in line!");
		}
		
	}
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);

		ArrayList<String> data = new ArrayList<String>();
		
		menu(input, data);
		
		input.close();
		
		

	}

}
