//Put multiple characters '.' into a string variable and print it out multiple time to get a square
package labs.lab0;

public class Lab0Square {

	public static void main(String[] args) {
		String top = ".........";
		System.out.println(top);
		System.out.println(top);
		System.out.println(top);
		System.out.println(top);
		System.out.println(top);
	}

}
//pseudocode did work out the way I expected as it shows a filled in square with the character '.'