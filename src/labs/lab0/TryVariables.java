package labs.lab0;

public class TryVariables {

	public static void main(String[] args) {
		// This example shows how to declare and initialize variables

		int integer = 100;
		long large = 42561230L;
		short small = 25; //need to put data type
		byte tiny = 19;

		float f = .0925F; //need to add semicolon
	    double decimal = 0.725;
		double largeDouble = +6.022E23; // This is in scientific notation

		char character = 'A';
		boolean t = true;

		System.out.println("integer is " + integer);
		System.out.println("large is " + large);
		System.out.println("small is " + small); //need to put capital s
		System.out.println("tiny is " + tiny); //wrong variable name
		System.out.println("f is " + f);
		System.out.println("decimal is " + decimal); //put . instead of comma
		System.out.println("largeDouble is " + largeDouble);
		System.out.println("character is " + character); //need to put +
		System.out.println("t is " + t);

	}

}