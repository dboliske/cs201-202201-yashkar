package labs.lab2;

import java.util.Scanner;

public class labTwoSquare {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		System.out.print("Enter size of square: ");
		
		double size = Double.parseDouble(input.nextLine());
		int x = 1;
		while (x<=size) {
			for (int count = 1; count<=size; count++) {
			System.out.print("* ");	//Each row 
			}
			//Each column
			System.out.println();
			x++;
		}

	}

}
