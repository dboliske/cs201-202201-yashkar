package labs.lab2;

import java.util.Scanner;

public class labTwoMenu {

	public static void main(String[] args) {
		
	Scanner input = new Scanner (System.in);
	
	boolean done = true; //initialize flag 
	
	while (done) {
		//Print menu options to user
		System.out.println("1. Say Hello");
		System.out.println("2. Addition");
		System.out.println("3. Multiplication");
		System.out.println("4. Exit");
		System.out.print("Choose Menu Option: ");
		
		String choice = input.nextLine(); //Prompt user for choice
		
		switch (choice) {
			case "1" :
				System.out.println("Hello");
				break;
		
			case "2":
				System.out.print("Enter Value One: ");
				double a = Double.parseDouble(input.nextLine());
				System.out.print("Enter Value Two: ");
				double b = Double.parseDouble(input.nextLine());
				
				double sum = a + b;
				
				System.out.println("The sume of the two numbers is " + sum);
				break;
			
			case "3":
				System.out.print("Enter Value One: ");
				double m = Double.parseDouble(input.nextLine());
				System.out.print("Enter Value Two: ");
				double n = Double.parseDouble(input.nextLine());
				
				double prod = m * n;
				
				System.out.println("The product of the two numbers is " + prod);
				break;
				
			case "4":
				done = false;
				break;
				
			default : 
				System.out.println("Did not enter valid menu option");
				
		}
		
	}
	input.close();
	}
}
		

