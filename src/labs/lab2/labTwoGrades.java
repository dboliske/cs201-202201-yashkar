package labs.lab2;

import java.util.Scanner;

public class labTwoGrades {
	public static void main(String[] args) {
		
		Scanner input = new Scanner (System.in);
		
		boolean done = true; //Initialize flag
		double sum = 0; //Initialize sum of grades
		int count = 0; //Initialize count of grades entered
		
		while (done) {
			//Prompt user to either enter a test score or enter -1 if they are finished
			System.out.print("Enter another Test Score, if finished enter '-1': ");
			double score = Double.parseDouble(input.nextLine());
			
			//If they enter -1, end loop
			if (score < 0) {
				done = false;
			//If test score is entered add it to sum
			} else {
			sum = sum + score; 
			count=  count +1; //keep count of how many test scores are entered
			}
		
		}	
		double average = (sum)/((double)(count)); //get average 
		System.out.println("The average of the test scores is " + average);
	input.close();	
	}
}

