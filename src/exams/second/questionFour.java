package exams.second;

public class questionFour {
	
public static void sort(String[] data) {
		
		for (int i=0; i<data.length; i++) {
			int min = i;
			
			for (int j=i+1; j<data.length; j++) {
				if (data[min].compareTo(data[j])>0) {
					min = j;
				}
			}
			
			if (min != i) {
				String temp = data[i];
				data[i] = data[min];
				data[min]=temp;
			}
		}
		
	}

	public static void main(String[] args) {
		
		String[] data = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};

		sort(data);
		
		for (int i = 0; i<data.length; i++) {
			System.out.print(data[i] + " ");
		}
		

	}

}
