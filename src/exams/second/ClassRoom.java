package exams.second;

public class ClassRoom {
	protected String building;
	protected String roomNumber;
	private int seats;
	
	public ClassRoom() {
		building = "Pritzer";
		roomNumber = "105";
		seats = 30;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		if (seats>0) {
			this.seats = seats;
		}
	}
	
	public String toString() {
		return "Class room is in " building + " room number " + roomNumber + " and has " + seats + " seats";
	}
	

}
