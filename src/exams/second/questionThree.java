package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class questionThree {
	
	public static void enterNumber(ArrayList<Integer> numbers, Scanner input) {
		
		boolean done = false;
		while (!done) {
		System.out.println("Enter a number or enter 'done': ");
		String ans = input.nextLine();
		try {
		if(!(ans.toLowerCase().equals("done"))) {
			int number = Integer.parseInt(ans);
			numbers.add(number);
		} else if(ans.toLowerCase().equals("done")) {
			done = true;
		} else {
			System.out.println("Not valid");
		}
	} catch (Exception e) {
		System.out.println("Not valid");
	}
		}
		
	}
	
	public static int min(ArrayList<Integer> numbers) {
		int min = numbers.get(0);
		for (int i=0; i<numbers.size(); i++) {
			if (numbers.get(i)<min) {
				min = numbers.get(i);
			}
		}
		return min;
	}
	
	public static int max(ArrayList<Integer> numbers) {
		int max = numbers.get(0);
		for (int i=0; i<numbers.size(); i++) {
			if (numbers.get(i)>max) {
				max = numbers.get(i);
			}
		}
		return max;
	}
	
	

	public static void main(String[] args) {
		
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		
		Scanner input = new Scanner(System.in);
		
		enterNumber(numbers, input);
		
		int min = min(numbers);
		int max = max(numbers);
		
		System.out.println("The max number entered is " + max + " and the min number entered is " + min);
		

	}

}
