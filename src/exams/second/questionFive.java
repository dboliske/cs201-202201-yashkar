package exams.second;

import java.util.Scanner;

public class questionFive {
	
	public static int search(double[] data, double value) {
		
		int interval = (int)Math.sqrt(data.length);
		int step = interval;
		
		int prev =0;
		
		while (data[Math.min(step, data.length - 1)]<value) {
			prev = step;
			step = step + interval;
			if (prev>= data.length) {
				 return -1;
			}
		}
		
		while (data[prev]<value) {
			prev++;
			if (prev == Math.min(step, data.length - 1)) {
				return -1;
			}
		}
		if (data[prev] == value) {
			return prev;
		}
		
		return -1;
	}
		
		
		
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		double[] data = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		System.out.print("Enter value: ");
		double value = Double.parseDouble(input.nextLine());
		int index =search(data, value);
		
		System.out.println(value + " occurs at index " + index);
	}

}
