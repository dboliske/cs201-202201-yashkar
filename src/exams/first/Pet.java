package exams.first;

public class Pet {
	
	private String name;
	private int age;
	
	public Pet() {
		name = "name";
		age = 0;
	}
	
	public Pet(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if (age >= 0) {
			this.age = age;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;	
	}
	public boolean equals(Pet p) {
		if (name.equals(p.getName())) {
			if (age == p.getAge()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public String toString() {
		return name + " is " + age + " years old";
	}
	
}
