package exams.first;

import java.util.Scanner;

public class QuestionOne {

	public static void main(String[] args) {
		
		Scanner input = new Scanner (System.in);
		
		System.out.print("Enter number: ");
		
		int number = Integer.parseInt(input.nextLine());
		
		int sum = number + 65;
		
		System.out.println("The number + 65 is " + sum);
		
		char let = (char)sum;
		
		System.out.println("The character for " + sum + " is " + let);
		
		input.close();
	}
}
