package exams.first;

import java.util.Scanner;

public class QuestionThree {

	public static void main(String[] args) {
		
		Scanner input = new Scanner (System.in);
		
		System.out.print("Enter number: ");
		
		int dim = Integer.parseInt(input.nextLine());
		
		int x=1;
		
		int space=dim;
		
		while (x<=dim) {

		for (int j=0; j<(space-dim);j++) {
			System.out.print("  ");
		}
		
		for (int i=0; i<dim; i++) {
			System.out.print("* ");
			
		}
		dim = dim-1;
		System.out.println();
			
		}
		
		
	}

}
